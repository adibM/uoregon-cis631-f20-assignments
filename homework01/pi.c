#include <stdlib.h> 
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include "common.h"
#include <inttypes.h>
#include <time.h>



#define PI 3.1415926535


void usage(int argc, char** argv);
double calcPi_Serial(int num_steps);
double calcPi_P1(int num_steps);
double calcPi_P2(int num_steps);
int threads_num;

int main(int argc, char** argv)
{
    // get input values
    // uint32_t num_steps = 100000;
    uint32_t num_steps = 100;
    if(argc > 1) {
        num_steps = atoi(argv[1]);
    } else {
        usage(argc, argv);
        printf("using %"PRIu32"\n", num_steps);
    }
    fprintf(stdout, "The first 10 digits of Pi are %0.10f\n", PI);
    uint32_t step = num_steps;

    double diff;
    double t1, t2, t3;
    // set up timer
    uint64_t start_t;
    uint64_t end_t;
    FILE *fp;
    fp = fopen("output.csv", "w+");
    fprintf(fp, "Threads,Steps,Difference,Serial Time, P1 Time, P2 Time\n");
    InitTSC();
    int threads[] = {1,2,3,5,10,20,30, 40, 50};
    // int threads[] = {1,2};
    for(int i =0;i<9; i++){
        // calculate in serial 
        num_steps = step*pow(10,i+1);
        for(int th=0;th < *(&threads+1)-threads; th++){
            threads_num = threads[th];
            start_t = ReadTSC();
            double Pi0 = calcPi_Serial(num_steps);
            end_t = ReadTSC();
            printf("Time to calculate Pi serially with %"PRIu32" steps is: %g\n",
                num_steps, ElapsedTime(end_t - start_t));
            printf("Pi is %0.10f\n", Pi0);
            diff = PI-Pi0;
            t1 = ElapsedTime(end_t - start_t);
            // calculate in parallel with reduce 
            start_t = ReadTSC();
            double Pi1 = calcPi_P1(num_steps);
            end_t = ReadTSC();
            printf("Time to calculate Pi in // with %"PRIu32" steps is: %g\n",
                num_steps, ElapsedTime(end_t - start_t));
            printf("Pi is %0.10f\n", Pi1);
            t2 = ElapsedTime(end_t - start_t);

            // calculate in parallel with atomic add
            start_t = ReadTSC();
            double Pi2 = calcPi_P2(num_steps);
            end_t = ReadTSC();
            printf("Time to calculate Pi in // + atomic with %"PRIu32" steps is: %g\n",
                num_steps, ElapsedTime(end_t - start_t));
            printf("Pi is %0.10f\n", Pi2);
            t3 = ElapsedTime(end_t - start_t);
            fprintf(fp, "%d,%d,%g,%g,%g,%g\n", threads_num, num_steps, diff, t1, t2, t3);
        }
    }    
    fclose(fp);
    return 0;
}


void usage(int argc, char** argv)
{
    fprintf(stdout, "usage: %s <# steps>\n", argv[0]);
}

double calcPi_Serial(int num_steps)
{
    double pi = 0.0;
    double width = 1.0/num_steps;
    int i;
    double x;
    double y;
    for(i = 1;i <= num_steps ; i++){
        x = i * width;
        y = sqrt(1-pow(x,2));
        pi += width*y;
    }
    return pi*4.0;
}

double calcPi_P1(int num_steps)
{
    double pi = 0.0;
    double width = 1.0/num_steps;
    int i;
    double x;
    double y;
    #pragma omp parallel for reduction(+:pi) num_threads(threads_num)
    for(i = 1;i <= num_steps ; i++){
        x = i * width;
        y = sqrt(1-pow(x,2));
        pi += width*y;
    }
    return pi*4.0;
}

double calcPi_P2(int num_steps)
{
    double pi = 0.0;
    double width = 1.0/num_steps;
    int i;
    double x;
    double y;
    #pragma omp parallel for num_threads(threads_num)
    for(i = 1;i <= num_steps ; i++){
        x = i * width;
        y = sqrt(1-pow(x,2));
        #pragma omp atomic
        pi += width*y;
    }
    return pi*4.0;
}
