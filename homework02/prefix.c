#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include "common.h"

void usage(int argc, char **argv);
void verify(int *sol, int *ans, int n);
void prefix_sum(int *src, int *prefix, int n);
void prefix_sum_p1(int *src, int *prefix, int n);
void prefix_sum_p2(int *src, int *prefix, int n);

int main(int argc, char **argv)
{
    // get inputs
    // uint32_t n = 8;
    uint32_t n = 1048576;
    unsigned int seed = time(NULL);
    if (argc > 2)
    {
        n = atoi(argv[1]);
        seed = atoi(argv[2]);
    }
    else
    {
        usage(argc, argv);
        printf("using %" PRIu32 " elements and time as seed\n", n);
    }

    // set up data
    int *prefix_array = (int *)AlignedMalloc(sizeof(int) * n);
    int *input_array = (int *)AlignedMalloc(sizeof(int) * n);
    srand(seed);
    for (int i = 1; i < n + 1; i++)
    {
        input_array[i - 1] = i;
        //input_array[i] = rand() % 100;
    }

    // set up timers
    uint64_t start_t;
    uint64_t end_t;
    InitTSC();

    // execute serial prefix sum and use it as ground truth
    start_t = ReadTSC();
    prefix_sum(input_array, prefix_array, n);

    printf("Expected output:");
    for (int i = 0; i < n; i++)
    {
        printf("%d ", prefix_array[i]);
    }
    printf("\n");

    end_t = ReadTSC();
    printf("Time to do O(N-1) prefix sum on a %" PRIu32 " elements: %g (s)\n",
           n, ElapsedTime(end_t - start_t));

    // execute parallel prefix sum which uses a NlogN algorithm
    int *input_array1 = (int *)AlignedMalloc(sizeof(int) * n);
    int *prefix_array1 = (int *)AlignedMalloc(sizeof(int) * n);
    memcpy(input_array1, input_array, sizeof(int) * n);
    start_t = ReadTSC();
    prefix_sum_p1(input_array1, prefix_array1, n);
    end_t = ReadTSC();
    printf("Time to do O(NlogN) //prefix sum on a %" PRIu32 " elements: %g (s)\n",
           n, ElapsedTime(end_t - start_t));
    verify(prefix_array, prefix_array1, n);

    // execute parallel prefix sum which uses a 2(N-1) algorithm
    memcpy(input_array1, input_array, sizeof(int) * n);
    memset(prefix_array1, 0, sizeof(int) * n);
    start_t = ReadTSC();
    prefix_sum_p2(input_array1, prefix_array1, n);
    end_t = ReadTSC();
    printf("Time to do 2(N-1) //prefix sum on a %" PRIu32 " elements: %g (s)\n",
           n, ElapsedTime(end_t - start_t));
    verify(prefix_array, prefix_array1, n);

    // free memory
    AlignedFree(prefix_array);
    AlignedFree(input_array);
    AlignedFree(input_array1);
    AlignedFree(prefix_array1);

    return 0;
}

void usage(int argc, char **argv)
{
    fprintf(stderr, "usage: %s <# elements> <rand seed>\n", argv[0]);
}

void verify(int *sol, int *ans, int n)
{
    int err = 0;
    for (int i = 0; i < n; i++)
    {
        if (sol[i] != ans[i])
        {
            err++;
        }
    }
    if (err != 0)
    {
        fprintf(stderr, "There was an error: %d\n", err);
    }
    else
    {
        fprintf(stdout, "Pass\n");
    }
}

void prefix_sum(int *src, int *prefix, int n)
{
    prefix[0] = src[0];
    for (int i = 1; i < n; i++)
    {
        prefix[i] = src[i] + prefix[i - 1];
    }
}

void prefix_sum_p1(int *src, int *prefix, int n)
{
    int log_n = log2(n);
    printf("log value %d\n", log_n);
    int i, j, two_i, index;

    for (int i = 0; i < n; i++)
    {
        prefix[i] = src[i];
    }
    int *dest = (int *)AlignedMalloc(sizeof(int) * n);
    for (i = 0; i <= log_n; i++)
    {
#pragma omp parallel for private(j) num_threads(500)
        for (j = 0; j < n; j++)
        {
            two_i = 1 << i;
            if (j >= two_i)
            {
                index = j - two_i;
                dest[j] = prefix[index] + prefix[j];
            }
            else
                dest[j] = prefix[j];
        }
        prefix = memcpy(prefix, dest, sizeof(int) * n);
    }
    AlignedFree(dest);
}

void prefix_sum_p2(int *src, int *prefix, int n)
{
    for (int i = 0; i < n; i++)
    {
        prefix[i] = src[i];
    }

    int log_n = log2(n);
    int i, j, temp, last, two_i, two_i_p1;

    for (i = 0; i < log_n; i++)
    {
        two_i_p1 = 1 << (i + 1);
        two_i = 1 << i;
#pragma omp parallel for private(j) num_threads(500)
        for (j = 0; j < n; j += two_i_p1)
        {
            prefix[j + two_i_p1 - 1] = prefix[j + two_i - 1] + prefix[j + two_i_p1 - 1];
        }
    }
    last = prefix[n - 1];

    prefix[n - 1] = 0;
    for (i = log_n - 1; i >= 0; i--)
    {
        two_i_p1 = 1 << (i + 1);
        two_i = 1 << i;
#pragma omp parallel for private(j, temp) num_threads(500)
        for (j = 0; j < n; j += (int)pow(2, i + 1))
        {
            temp = prefix[j + two_i - 1];
            prefix[j + two_i - 1] = prefix[j + two_i_p1 - 1];
            prefix[j + two_i_p1 - 1] = temp + prefix[j + two_i_p1 - 1];
        }
    }
    for (j = 0; j < n - 1; j++)
    {
        prefix[j] = prefix[j + 1];
    }

    prefix[n - 1] = last;
}
